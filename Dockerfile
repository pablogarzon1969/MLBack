FROM python:3

RUN apt-get update
RUN apt-get install -y liblapack-dev libblas-dev gfortran
RUN pip3 install pandas
RUN pip3 install NumPy
RUN pip3 install SciPy
RUN pip3 install sklearn

WORKDIR /app

COPY . /app

RUN pip --no-cache-dir install -r requirements.txt

EXPOSE 80

ENTRYPOINT ["python3"]
CMD ["app.py"]