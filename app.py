from flask import Flask, request, jsonify
from flask_cors import CORS
from sklearn import svm
from sklearn import datasets
#Paquete entrenamiento del modelo
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib
import numpy as np
import pandas as pd
#Paquete para graficar
import matplotlib.pyplot as plt

# initialize flask application
app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/api/testone', methods=['GET'])
def testone():
	return jsonify({'mensaje': "hola"})